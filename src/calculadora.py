import sys
import os.path
import pytest

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
#sys.path.append(os.path.join(os.path.dirname(__file__)))
#print(os.path.dirname(__file__), '..')
#print(os.path.join(os.path.dirname(__file__), '..'))
#print(__import__('pkg_resources').declare_namespace(__name__))

from src.somar import somar
from src.subtrair import subtrair
from src.multiplicar import multiplicar
from src.dividir import dividir

def test_func():
    assert somar.somar(1,2) == 3
    assert subtrair.subtrair(1,2) == -1
    assert multiplicar.multiplicar(1,2) == 2
    assert dividir.dividir(1,2) == .5

def test_menu():
    with pytest.raises(IOError) as e:
        menu()
    e.match(r'reading from stdin while output is captured')
    #assert menu() == '>>>'

def adicao():
    print('\n\nsoma')
    a = input('#1: ')
    b = input('#2: ')
    print(somar.somar(a, b))

def subtracao():
    print('ok')
    pass

def multiplicacao():
    print('ok')
    pass

def divisao():
    print('ok')
    pass

def menu():
    itens = ['exit', 'sair', '0', 'e', 's']


    op = None
    while True:
        print('\n\nCalculadora')
        print('escolha a operação:')
        print('1 - somar(+)')
        print('2 - subtrair (-)')
        print('3 - multiplicar (*)')
        print('4 - dividir (/)')
        print('0 - Sair (S, E, exit)')
        op = input('>>> ')
        if op.lower() in itens:
            exit(0)
        elif op == '1' or op == '+':
            adicao()
        elif op == '2' or op == '-':
            subtracao()
        elif op == '3' or op == '*':
            multiplicacao()
        elif op == '4' or op == '/':
            divisao()
        else:
            pass

    pass

def main():
    menu()
    pass

if __name__ == '__main__':
    main()